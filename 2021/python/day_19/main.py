import math
import numpy as np
import matplotlib.pyplot as plt


def parselines(chars: str):
    scanners = chars.split("\n\n")

    def parsescanner(scanner: str):
        lines = scanner.split("\n")
        titlestr, *coords = lines
        title = titlestr.split(" ")[2]

        def parsecoords(coords: str):
            return list(map(int, coords.split(",")))
        return (title, list(map(parsecoords, coords)))
    return list(map(parsescanner, scanners))


def distmatrix(scc: list) -> np.ndarray:
    """
    takes a list of n-d points and returns the euklidian distance
    in space between 2 points for all of them pairwise
    """
    def dist(p1, p2):
        return math.sqrt(sum(map(lambda x: (x[0] - x[1])**2, zip(p1, p2))))

    return np.array(list(map(lambda p1: list(map(lambda p2: dist(p1, p2), scc)), scc)))


def find_overlaps(dm1: np.ndarray, dm2: np.ndarray, n_overlaps: int = 4) -> tuple[int, int]:
    def _equal_vals(v1: list, v2: list):
        return sum(1 if foo in v2 else 0 for foo in v1)

    def _count_overlaps(d1):
        return list(map(lambda d2: _equal_vals(d1, d2), dm2))

    overlaps = np.array(list(map(_count_overlaps, dm1)))
    return np.where(overlaps >= n_overlaps)


def solve_rot_offset(refpoints: np.ndarray, scannerpoints: np.ndarray) -> tuple[np.ndarray, np.ndarray]:
    # TODO; this sucks
    # rotation * scannerpoint + offset = refpoint
    #
    #  [r11, r12, r13]     [x1]     [o1]     [y1]
    #  [r21, r22, r23]  @  [x2]  +  [o2]  =  [y2]
    #  [r31, r32, r33]     [x3]     [o3]     [y3]
    #
    #    y1 = r11 * x1 + r12 * x2 + r13 * x3 + o1
    # => y2 = r21 * x1 + r22 * x2 + r23 * x3 + o2
    #    y3 = r31 * x1 + r32 * x2 + r33 * x3 + o3
    #
    #                        |
    #                       \/
    #
    #                                           [r11]
    #                                           [r12]
    #                                           [r13]
    #     [x1 x2 x3 0  0  0  0  0  0 1 0 0]     [r21]     [y1]
    #     [0  0  0 x1 x2 x3  0  0  0 0 1 0]  @  [r22]  =  [y2]
    #     [0  0  0  0  0  0 x1 x2 x3 0 0 1]     [r23]     [y3]
    #                                           [r31]
    #                                           [r32]
    #                                           [r33]
    #                                           [o1]
    #                                           [o2]
    #                                           [o3]
    #

    rp = refpoints[:]
    sp = scannerpoints[:]
    nop = sp.shape[0]

    a = np.zeros((np.prod(rp.shape), 12))
    b = np.zeros((np.prod(rp.shape)))

    for i, p in enumerate(rp):
        a[i + 0 * nop, 0:3] = p
        a[i + 1 * nop, 3:6] = p
        a[i + 2 * nop, 6:9] = p
        a[i + 0 * nop, 9] = 1
        a[i + 1 * nop, 10] = 1
        a[i + 2 * nop, 11] = 1

    for i, p in enumerate(sp):
        b[i::nop] = p

    x, *_ = np.linalg.lstsq(a, b, rcond=None)
    rot = np.linalg.inv(x[:-3].reshape((3, 3)))
    offset = x[-3:]
    return rot, rot @ offset


def part1(scanners: list[tuple[str, list]]) -> int:
    # start with the points of scanner 0
    # centered at 0 for reference

    # position of scanners
    scanps = {0: [0, 0, 0]}
    curpoints = np.array([[0, 0, 0]])
    curpoints = np.unique(np.append(curpoints, scanners[0][1], axis=0), axis=0)
    print(len(curpoints))

    scnr = 0

    while len(scanps) < len(scanners):
        curdm = distmatrix(curpoints)
        print("solved: ", curdm.shape)

        # to preserve the variables past the loop
        i1, i2 = None, None

        # find the first scanner with 12 overlapping points
        save = None, [], None, None
        for scnr, scanner in enumerate(scanners[1:], 1):
            if scnr in scanps.keys():
                continue
            dm2 = distmatrix(scanner[1])
            i1, i2 = find_overlaps(curdm, dm2)

            print(f"scanner {scnr}: {len(i1)} overlaps")

            odm2, oi1, oi2, oscnr = save
            if len(i1) > len(oi1):
                save = dm2, i1, i2, scnr

        dm2, i1, i2, scnr = save

        print("next scnr:", scnr)
        if scnr is None:
            print("Something went wrong!")
            exit(1)

        refpoints = curpoints[i1]
        scannerpoints = np.array(scanners[scnr][1])
        rot, offset = solve_rot_offset(refpoints, scannerpoints[i2])

        # add the newly found points to the curpoints and continue
        extrapoints = [rot @ scanp + offset for scanp in scannerpoints]
        curpoints = np.unique(np.append(curpoints, extrapoints, axis=0), axis=0)

        # save the seanners positions (offsets) in scanps!
        scanps[scnr] = -1 * offset
        print(len(curpoints))
        print(scanps)
        print("-------------")

    # return the number of beacons
    return len(curpoints)


def main():
    np.set_printoptions(linewidth=400)

    with open("2021/day_19/tinput.txt") as f:
        chars = f.read().strip()
    inp = parselines(chars)

    print(part1(inp))


if __name__ == '__main__':
    main()
