from functools import reduce


def parseline(line: str):
    inst, pos = line.strip().split(" ")
    xyz = map(lambda x: x.split("=")[-1], pos.split(","))
    ranges = list(map(lambda x: list(map(int, x.split(".."))), xyz))
    return inst, ranges


def part1(inp: list[str]):
    def setreactor(lot: dict, instruction, init: bool = True):
        inst, pos = instruction
        xr, yr, zr = pos
        if init:
            for x in range(max(xr[0], -50), 1 + min(xr[1], 50)):
                for y in range(max(yr[0], -50), 1 + min(yr[1], 50)):
                    for z in range(max(zr[0], -50), 1 + min(zr[1], 50)):
                        lot[(x, y, z)] = inst
        else:
            for x in range(*xr):
                for y in range(*yr):
                    for z in range(*zr):
                        if inst == "off":
                            lot.pop((x, y, z), 0)
                        else:
                            lot[(x, y, z)] = 1

        return lot
    out = reduce(setreactor, inp, {})
    return len(list(filter(lambda x: x == "on", out.values())))


def main():
    with open("2021/day_22/input.txt") as f:
        lines = f.readlines()
    inp = list(map(parseline, lines))

    print(part1(inp))


if __name__ == '__main__':
    main()
