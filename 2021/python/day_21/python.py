from itertools import cycle, count, product
from collections import Counter
from typing import Tuple
from functools import lru_cache
from time import perf_counter


# Part 1
def deterministic_dice(sides: int, count: int) -> int:
    sides = cycle(range(1, sides + 1))
    while True:
        yield sum(next(sides) for _ in range(count))


def part1(initial_position):
    position = initial_position.copy()
    score = Counter()
    d100 = deterministic_dice(100, 3)
    players = cycle(position)
    dice_count = count(3, 3)

    # Simulate the game
    for player, roll, dice_count in zip(players, d100, dice_count):
        position[player] = (
            position[player] + roll
        ) % 10
        score.update({player: position[player] + 1})
        if score[player] >= 1000:
            break

    min_score = min(score.values())
    return min_score * dice_count


# Part 2
def part2(initial_position, scorelimit: int = 21):
    dice_sums = Counter(sum(rolls) for rolls in product((1, 2, 3), repeat=3))

    @lru_cache(maxsize=None)
    def dirac_dice(
        p1_pos: int, p2_pos: int, p1_score: int = 0, p2_score: int = 0
    ) -> Tuple[int, int]:

        if p1_score >= scorelimit:
            return (1, 0)
        if p2_score >= scorelimit:
            return (0, 1)

        # Cumulative amount of wins for each player
        p1_wins_total = 0
        p2_wins_total = 0

        # Iterate over all possible rolls in a turn
        for roll, amount in dice_sums.items():

            # Move the player and increment their score
            p1_pos_new = (p1_pos + roll) % 10
            p1_score_new = p1_score + p1_pos_new + 1

            # Pass the turn to the other player
            p2_wins, p1_wins = dirac_dice(p2_pos, p1_pos_new, p2_score, p1_score_new)

            # Increment the win counter by the results multiplied by the amount of times the roll repeats
            p1_wins_total += p1_wins * amount
            p2_wins_total += p2_wins * amount

        # Returns the final tally of victories
        return (p1_wins_total, p2_wins_total)

    return dirac_dice(p1_pos=initial_position[1], p2_pos=initial_position[2])


def main():

    # with open("2021/day_21/input.txt", "rt") as file:
        # initial_position = {}
        # for player, line in enumerate(file, start=1):
            # initial_position[player] = int(line.split()[-1]) - 1

    initial_position = {
        1: 4,
        2: 8
    }

    start_time = perf_counter()

    p1score = part1(initial_position)
    p2score = part2(initial_position, scorelimit=2)

    end_time = perf_counter()

    print(f"Part 1: {p1score}")
    print(f"Part 2: {p2score}")
    print(f"Time: {end_time - start_time:.3f} s")


if __name__ == '__main__':
    main()
