import numpy as np
import matplotlib.pyplot as plt
import copy


def toarr(cucumbers: list):
    return np.array(list(map(lambda row: list(map(lambda x: {".": 0, ">": 1, "v": 2}[x], row)), cucumbers)))


def nextmove(oldcumbers: list, debug: bool = False, moves: int = 1):
    cucumbers = copy.deepcopy(oldcumbers)
    maxx, maxy = len(oldcumbers[0]), len(oldcumbers)
    moved = False

    if moves == 1 and debug:
        print(np.array(cucumbers))

    # move east-facing cucumbers first
    for y, row in enumerate(cucumbers):
        for x, cucumber in enumerate(row):
            if oldcumbers[y][x] == ">" and oldcumbers[y][(x+1) % maxx] == ".":
                cucumbers[y][x], cucumbers[y][(x+1) % maxx] = ".", ">"
                moved = True

    medcumber = copy.deepcopy(cucumbers)

    for y, row in enumerate(cucumbers):
        for x, cucumber in enumerate(row):
            if medcumber[y][x] == "v" and medcumber[(y+1) % maxy][x] == ".":
                cucumbers[y][x], cucumbers[(y+1) % maxy][x] = ".", "v"
                moved = True

    if debug:
        print(f"after {moves} steps")
        print(np.array(cucumbers))
        input()

    if not moved:
        return moves
    else:
        return nextmove(cucumbers, moves=moves + 1)


def main():
    with open("2021/day_25/input.txt") as f:
        chars = f.read().strip()

    cucumbers = list(map(lambda x: list(x), chars.split()))
    # cuc_test = [list("...>>>>>...")]
    print(nextmove(cucumbers))

    # 229 too low


if __name__ == '__main__':
    main()
