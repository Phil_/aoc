import matplotlib.pyplot as plt
import numpy as np


def main():
    data = np.loadtxt("./coords.txt", delimiter=",")
    plt.scatter(data[:, 0], data[:, 1], c=data[:, 2])
    plt.colorbar()
    plt.xlabel("dx")
    plt.ylabel("dy")
    plt.title("Max height by dx/dy starting velocity")
    plt.show()


if __name__ == '__main__':
    main()
