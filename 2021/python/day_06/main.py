import numpy as np
from numpy.linalg import matrix_power


def main():
    # with open("2021/day_06/input.txt") as file:
    # start = list(map(int, file.readline()[:-1].split(",")))

    start = [3, 4, 3, 1, 2]

    # days = 9999999
    days = 67108864
    # m = np.eye(9, k=1, dtype=np.int64)
    m = np.eye(9, k=1, dtype=object)
    m[6, 0] = 1
    m[8, 0] = 1

    fishTimer = np.histogram(start, bins=range(10))[0]
    fishTimer = matrix_power(m, days).dot(fishTimer)
    print(np.sum(fishTimer))


if __name__ == "__main__":
    main()
