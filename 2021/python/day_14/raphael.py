import numpy as np
from collections import defaultdict


def main():
    letters = []  # speichert einzelne buchstaben
    reactions = defaultdict(list)  # speichert {speziespaar: [beiden entstehende paare]}
    pairs = []  # alle speziespaare, also keys von reactions
    pairLetter = (
        {}
    )  # speichert übergänge wie in datei, {speziespaar: zwischenbuchstabe}

    steps = 1

    # with open("test_day14.txt", "r") as file:
    with open("2021/day_14/input.txt", "r") as file:
        lines = file.readlines()
    startpolymer = lines[0][:-1]  # start polymer

    # listen und dicts füllen
    for line in lines[2:]:
        left, right = line[:-1].split(" -> ")
        pairLetter[left] = right
        letters.extend(list(left))
        letters.extend(list(right))
        if left not in pairs:
            pairs.append(left)
        pair1 = left[0] + right
        if pair1 not in pairs:
            pairs.append(pair1)
        reactions[left].append(pair1)
        pair2 = right + left[1]
        if pair2 not in pairs:
            pairs.append(pair2)
        reactions[left].append(pair2)


    letters = list(set(letters))
    letterCounts = defaultdict(int)  # speichert {buchstabe: anzahl}
    for key in letters:  # zählt buchstaben vom start polymer
        letterCounts[key] = startpolymer.count(key)

    print(reactions)
    exit(0)

    # übergangsmatrix erstellen und besetzen
    m = np.zeros((len(pairs), len(pairs)), dtype=object)
    for key, value in reactions.items():
        i = pairs.index(key)
        for pair in value:
            j = pairs.index(pair)
            m[i][j] = 1

    # startvektor mit speziespaaren
    start = np.zeros((len(pairs)), dtype=object)
    for i in range(len(startpolymer)):
        pair = startpolymer[i - 1 : i + 1]
        if pair in pairs:
            start[pairs.index(pair)] = 1

    res = start
    # vektor matrixprodukt step mal
    for _ in range(steps):
        # zählt anhand des zwischenergnissvektors wieviele buchstaben jeweils hinzukommen
        for key, value in pairLetter.items():
            letterCounts[value] += res[pairs.index(key)]
        print(letterCounts)
        res = res.dot(m)

    print(sorted(pairs))
    print(len(pairs))

    print(dict(filter(lambda e: e[1] > 0, zip(pairs, res))))

    maxi = np.amax(list(letterCounts.values()))
    mini = np.amin(list(letterCounts.values()))
    print("Most common minus least common:", maxi - mini)


if __name__ == "__main__":
    main()
