# ## --- Day 9: Smoke Basin ---
# 
# These caves seem to be [lava
# tubes](https://en.wikipedia.org/wiki/Lava_tube). Parts are even still
# volcanically active; small hydrothermal vents release smoke into the
# caves that slowly [settles like
# rain]{title="This was originally going to be a puzzle about watersheds, but we're already under water."}.
# 
# If you can model how the smoke flows through the caves, you might be
# able to avoid it and be that much safer. The submarine generates a
# heightmap of the floor of the nearby caves for you (your puzzle input).
# 
# Smoke flows to the lowest point of the area it's in. For example,
# consider the following heightmap:
# 
#     2199943210
#     3987894921
#     9856789892
#     8767896789
#     9899965678
# 
# Each number corresponds to the height of a particular location, where
# `9` is the highest and `0` is the lowest a location can be.
# 
# Your first goal is to find the *low points* - the locations that are
# lower than any of its adjacent locations. Most locations have four
# adjacent locations (up, down, left, and right); locations on the edge or
# corner of the map have three or two adjacent locations, respectively.
# (Diagonal locations do not count as adjacent.)
# 
# In the above example, there are *four* low points, all highlighted: two
# are in the first row (a `1` and a `0`), one is in the third row (a `5`),
# and one is in the bottom row (also a `5`). All other locations on the
# heightmap have some lower adjacent location, and so are not low points.
# 
# The *risk level* of a low point is *1 plus its height*. In the above
# example, the risk levels of the low points are `2`, `1`, `6`, and `6`.
# The sum of the risk levels of all low points in the heightmap is
# therefore `15`.
# 
# Find all of the low points on your heightmap. *What is the sum of the
# risk levels of all low points on your heightmap?*

#(def tchars (slurp "2021/day_09/tinput.txt"))
#(def tchars (string/slice tchars 0 (- (length tchars) 1)))
#(def tlines (string/split "\n" tchars))

(import aoc)
(setdyn :year 2021)
(setdyn :day 09)
(def lines (aoc/daylines))

(defn transpose [arr]
  (map
    (fn [idx] (map (fn [row] (get row idx)) arr))
    (range 0 (length (get arr 0)))))


(defn aprint [arr]
  (->> arr (map string) (interpose " ") string/join print))


(defn pad1d [arr]
  (array/concat @[9] arr [9]))


(defn pad2d [arr]
  (->>
    arr
    (map pad1d)
    transpose
    (map pad1d)
    transpose))


(defn parseLines [ilines] 
  (->>
    ilines
    (map string/bytes)
    (map (fn [line] (map (fn [x] (- x 48)) line)))))

(defn diff [line]
  (let [l (length line)]
    (->> 
      (array/concat @[] [(array/slice line 0 (- l 1))] [(array/slice line 1 l)])
      transpose
      (map (fn [[prev this]] (- this prev))))))

(defn analine [line]
  (let [l (length line)]
    (->>
      (array/concat @[] [(array/slice line 0 (- l 1))] [(array/slice line 1 l)])
      transpose
      (map (fn [[prev this]] (and (< prev 0) (> this 0)))))))

#(aprint (diff [9 9 9 68 9 8 9 9]))

(def vals (parseLines lines))

(def dx (->> 
  vals
  (map pad1d)
  (map diff)
  (map analine)))

(def dy (->>
  vals
  transpose
  (map pad1d)
  (map diff)
  (map analine)
  transpose))

(defn indxy [arr]
  (mapcat
    (fn [rowi]
      (map 
        (fn [coli] [coli rowi (get (get arr rowi) coli)])
        (range (length (get arr rowi)))))
    (range (length arr))))

(def points 
  (map 
    (fn [[[x y val] [x y x?] [x y y?]]] [x y (and x? y?) val])
    (transpose (array/concat @[] [(indxy vals)] [(indxy dx)] [(indxy dy)]))))


(prin "part 1: ")
(->>
  points
  (filter (fn [[x y lowpoint? val]] lowpoint?))
  (map (fn [[x y lowpoint? val]] (+ 1 val)))
  sum
  print)


# part 2

(defn in? [arr val]
  (->> arr (count (fn [x] (= x val))) (< 0)))

(def n_basins (count (fn [[x y lowpoint? val]] lowpoint?) points))


(defn floodfill [[lot curid points] [x y lowpoint val]]
  (if lowpoint (print curid " / " n_basins))
  (if (and
        (not= val nil)
        (not= val 9)
        (not (in? (keys lot) [x y])))
    (do
      (set (lot [x y]) curid)

      (reduce
        (fn [nlot [nx ny]]
          (get (floodfill 
            [nlot curid points]
            [nx ny false (get-in vals [ny nx])]) 0))
        lot
        [
         [(inc x) y]
         [(dec x) y]
         [x (inc y)]
         [x (dec y)]
        ])
      [lot (inc curid) points])
    [lot curid points]))


(prin "part 2: ")
(->
  (reduce
    floodfill
    [@{} 1 points]
    (filter (fn [[x y lowpoint val]] lowpoint) points))
  (get 0)
  values
  frequencies 
  values
  sorted
  reverse
  ((fn [arr] (take 3 arr)))
  product
  print)
