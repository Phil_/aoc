# ## --- Day 14: Extended Polymerization ---
# 
# The incredible pressures at this depth are starting to put a strain on
# your submarine. The submarine has
# [polymerization](https://en.wikipedia.org/wiki/Polymerization) equipment
# that would produce suitable materials to reinforce the submarine, and
# the nearby volcanically-active caves should even have the necessary
# input elements in sufficient quantities.
# 
# The submarine manual contains [instructions]{title="HO
# 
# HO -> OH"} for finding the optimal polymer formula; specifically, it
# offers a *polymer template* and a list of *pair insertion* rules (your
# puzzle input). You just need to work out what polymer would result after
# repeating the pair insertion process a few times.
# 
# For example:
# 
#     NNCB
# 
#     CH -> B
#     HH -> N
#     CB -> H
#     NH -> C
#     HB -> C
#     HC -> B
#     HN -> C
#     NN -> C
#     BH -> H
#     NC -> B
#     NB -> B
#     BN -> B
#     BB -> N
#     BC -> B
#     CC -> N
#     CN -> C
# 
# The first line is the *polymer template* - this is the starting point of
# the process.
# 
# The following section defines the *pair insertion* rules. A rule like
# `AB -> C` means that when elements `A` and `B` are immediately adjacent,
# element `C` should be inserted between them. These insertions all happen
# simultaneously.
# 
# So, starting with the polymer template `NNCB`, the first step
# simultaneously considers all three pairs:
# 
# -   The first pair (`NN`) matches the rule `NN -> C`, so element `C` is
#     inserted between the first `N` and the second `N`.
# -   The second pair (`NC`) matches the rule `NC -> B`, so element `B` is
#     inserted between the `N` and the `C`.
# -   The third pair (`CB`) matches the rule `CB -> H`, so element `H` is
#     inserted between the `C` and the `B`.
# 
# Note that these pairs overlap: the second element of one pair is the
# first element of the next pair. Also, because all pairs are considered
# simultaneously, inserted elements are not considered to be part of a
# pair until the next step.
# 
# After the first step of this process, the polymer becomes `NCNBCHB`.
# 
# Here are the results of a few steps using the above rules:
# 
#     Template:     NNCB
#     After step 1: NCNBCHB
#     After step 2: NBCCNBBBCBHCB
#     After step 3: NBBBCNCCNBBNBNBBCHBHHBCHB
#     After step 4: NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB
# 
# This polymer grows quickly. After step 5, it has length 97; After step
# 10, it has length 3073. After step 10, `B` occurs 1749 times, `C` occurs
# 298 times, `H` occurs 161 times, and `N` occurs 865 times; taking the
# quantity of the most common element (`B`, 1749) and subtracting the
# quantity of the least common element (`H`, 161) produces
# `1749 - 161 = 1588`.
# 
# Apply 10 steps of pair insertion to the polymer template and find the
# most and least common elements in the result. *What do you get if you
# take the quantity of the most common element and subtract the quantity
# of the least common element?*


(import aoc)
(setdyn :year 2021)
(setdyn :day 14)
(def lines (string/split "\n\n" (aoc/daychars)))

#(def tchars (string/trim (slurp "2021/day_14/tinput.txt")))
#(def tlines (string/split "\n\n" tchars))

(defn getlots [polymer]
  (map
    frequencies 
    [polymer
     (->>
       (range (dec (length polymer)))
       (map |[$ (inc $)])
       (aoc/map2d |(get polymer $))
       (map |(tuple ;$)))]))


# parse the instrucions from string to table
(defn parseinsts [instructions]
  (->>
    instructions
    (string/split "\n")
    (map |(string/split " -> " $))
    (map (fn [[lhs rhs]] [(string/bytes lhs) rhs]))
    (mapcat identity)
    (|(table ;$))))


(defn iterate [[plot pairlot instructions] nday]
  # for every rule in "instructions" filter the fitting pairs in pairlot
  (var nplot plot)
  (var npairlot @{})

  (->>
    pairlot
    keys
    (map
      (fn [pair]
        (def newp (->>
            (tuple ;pair)
            (get instructions)
            string/bytes
            first))

        # increment the count of that p in plot by the number of pairs affected
        (set (nplot newp) (+ (get pairlot pair 0) (get nplot newp 0)))

        # set the counter of the 2 new pairs in the pairlot
        (def newps (interpose newp pair))

        (def k1 (tuple ;(array/slice newps 0 2)))
        (def k2 (tuple ;(array/slice newps 1 3)))

        # get the old number of pairs -> every pair
        (def npair (get pairlot pair 0))

        (set (npairlot k1) (+ npair (get npairlot k1 0)))
        (set (npairlot k2) (+ npair (get npairlot k2 0))))))
  [nplot npairlot instructions])


(defn solve [lines ndays]
  (let [startpolymer (get lines 0)
        instructions (parseinsts (get lines 1))]
    (def [endpolylot]
      (reduce
        iterate
        [;(getlots startpolymer) instructions]
        (range 1 (inc ndays))))
    (-
      (max-of (values endpolylot))
      (min-of (values endpolylot)))))


# part 1
(prin "part 1: ")
(pp (solve lines 10))

# part 2
(prin "part 2: ")
(pp (solve lines 40))
