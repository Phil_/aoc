# ## --- Day 4: Giant Squid ---
# 
# You're already almost 1.5km (almost a mile) below the surface of the
# ocean, already so deep that you can't see any sunlight. What you *can*
# see, however, is a giant squid that has attached itself to the outside
# of your submarine.
# 
# Maybe it wants to play
# [bingo](https://en.wikipedia.org/wiki/Bingo_(American_version))?
# 
# Bingo is played on a set of boards each consisting of a 5x5 grid of
# numbers. Numbers are chosen at random, and the chosen number is *marked*
# on all boards on which it appears. (Numbers may not appear on all
# boards.) If all numbers in any row or any column of a board are marked,
# that board *wins*. (Diagonals don't count.)
# 
# The submarine has a *bingo subsystem* to help passengers (currently, you
# and the giant squid) pass the time. It automatically generates a random
# order in which to draw numbers and a random set of boards (your puzzle
# input). For example:
# 
#     7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1
# 
#     22 13 17 11  0
#      8  2 23  4 24
#     21  9 14 16  7
#      6 10  3 18  5
#      1 12 20 15 19
# 
#      3 15  0  2 22
#      9 18 13 17  5
#     19  8  7 25 23
#     20 11 10 24  4
#     14 21 16 12  6
# 
#     14 21 17 24  4
#     10 16 15  9 19
#     18  8 23 26 20
#     22 11 13  6  5
#      2  0 12  3  7
# 
# After the first five numbers are drawn (`7`, `4`, `9`, `5`, and `11`),
# there are no winners, but the boards are marked as follows (shown here
# adjacent to each other to save space):
# 
#     22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
#      8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
#     21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
#      6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
#      1 12 20 15 19        14 21 16 12  6         2  0 12  3  7
# 
# After the next six numbers are drawn (`17`, `23`, `2`, `0`, `14`, and
# `21`), there are still no winners:
# 
#     22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
#      8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
#     21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
#      6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
#      1 12 20 15 19        14 21 16 12  6         2  0 12  3  7
# 
# Finally, `24` is drawn:
# 
#     22 13 17 11  0         3 15  0  2 22        14 21 17 24  4
#      8  2 23  4 24         9 18 13 17  5        10 16 15  9 19
#     21  9 14 16  7        19  8  7 25 23        18  8 23 26 20
#      6 10  3 18  5        20 11 10 24  4        22 11 13  6  5
#      1 12 20 15 19        14 21 16 12  6         2  0 12  3  7
# 
# At this point, the third board *wins* because it has at least one
# complete row or column of marked numbers (in this case, the entire top
# row is marked: `14 21 17 24  4`).
# 
# The *score* of the winning board can now be calculated. Start by finding
# the *sum of all unmarked numbers* on that board; in this case, the sum
# is `188`. Then, multiply that sum by *the number that was just called*
# when the board won, `24`, to get the final score, `188 * 24 = 4512`.
# 
# To guarantee victory against the giant squid, figure out which board
# will win first. *What will your final score be if you choose that
# board?*

# test boards
(def testboards [
  [[1 2 3] [4 5 6] [7 8 9]]
  [[0 0 0] [4 5 6] [7 8 9]]
  [[1 2 3] [0 0 0] [7 8 9]]
  [[1 2 3] [4 5 6] [0 0 0]]
  [[0 2 3] [0 5 6] [0 8 9]]
  [[1 0 3] [4 0 6] [7 0 9]]
  [[1 2 0] [4 5 0] [7 8 0]]
])
(def t0 (get testboards 0))

(defn printb [board]
  (each row board (print ;row)))

# read full file to string
(import aoc)
(setdyn :year 2021)
(setdyn :day 4)
(def lines (string/split "\n\n" (aoc/daychars)))

# get the draws from the first line
(def draws (map int/u64 (string/split "," (get lines 0))))

# parse the boards
(def boardstrings (array/slice lines 1 (length lines)))

(defn parserow [rowstring]
  (map 
    int/u64
    (filter 
      (fn [val] (not (or (= val " ") (= val ""))))
      (string/split " " rowstring))))

(defn parseboard [boardstring]
  (map parserow (string/split "\n" boardstring)))

(def boards (map parseboard boardstrings))

# check wether board is winning
(defn checkrow [row] (= (sum row) 0))

(defn transpose [board]
  (map 
    (fn [idx] (map (fn [row] (get row idx)) board))
    (range 0 (length (get board 0)))))

(defn checkboard [board]
  (def checks (array/concat @[]
    (map checkrow board) 
    (map checkrow (transpose board))))
  (> (length (filter (fn [x] x) checks)) 0))

(defn score [board move]
  (* move (sum (map sum board))))

(defn markmove [board move]
  (map 
    (fn [row]
      (map 
        (fn [val]
          (if (= val move) 0 val))
        row))
    board))

(def [_ scores] (reduce 
  (fn [[boards scores] draw]
    (def drawboards (map
      (fn [board]
        (markmove board draw))
      boards))

    (def wonboards (filter checkboard drawboards))
    
    (def nextboards (filter (fn [board] (not (checkboard board))) drawboards))
    (def scores [;scores ;(map (fn [board] (score board draw)) wonboards)])

    [nextboards scores])
  [boards []]
  draws))

# part 1
(prin "part 1: ")
(print (get scores 0))

# part 2
(prin "part 2: ")
(print (get scores (- (length scores) 1)))
