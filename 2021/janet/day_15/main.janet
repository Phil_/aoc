# ## --- Day 15: Chiton ---
# 
# You've almost reached the exit of the cave, but the walls are getting
# closer together. Your submarine can barely still fit, though; the main
# problem is that the walls of the cave are covered in
# [chitons](https://en.wikipedia.org/wiki/Chiton), and it would be best
# not to bump any of them.
# 
# The cavern is large, but has a very low ceiling, restricting your motion
# to two dimensions. The shape of the cavern resembles a square; a quick
# scan of chiton density produces a map of *risk level* throughout the
# cave (your puzzle input). For example:
# 
#     1163751742
#     1381373672
#     2136511328
#     3694931569
#     7463417111
#     1319128137
#     1359912421
#     3125421639
#     1293138521
#     2311944581
# 
# You start in the top left position, your destination is the bottom right
# position, and you [cannot move
# diagonally]{title="Can't go diagonal until we can repair the caterpillar unit. Could be the liquid helium or the superconductors."}.
# The number at each position is its *risk level*; to determine the total
# risk of an entire path, add up the risk levels of each position you
# *enter* (that is, don't count the risk level of your starting position
# unless you enter it; leaving it adds no risk to your total).
# 
# Your goal is to find a path with the *lowest total risk*. In this
# example, a path with the lowest total risk is highlighted here:
# 
#     1163751742
#     1381373672
#     2136511328
#     3694931569
#     7463417111
#     1319128137
#     1359912421
#     3125421639
#     1293138521
#     2311944581
# 
# The total risk of this path is `40` (the starting position is never
# entered, so its risk is not counted).
# 
# *What is the lowest total risk of any path from the top left to the
# bottom right?*

(import aoc)
(setdyn :year 2021)
(setdyn :day 15)
(def lines (aoc/daylines))

#(def tchars (string/trim (slurp "2021/day_15/tinput.txt")))
#(def tlines (string/split "\n" tchars))


(defn plines [lines]
  (->>
    lines
    (map aoc/singlechars)
    (aoc/map2d scan-number)))

(defn incwrap [arr times]
  (->>
    arr
    (aoc/map2d |(mod (+ $ times) 10))
    (aoc/map2d |(if (= $ 0) 1 $))))


(defn expandarr [arr]
  (let [oldx (length (get arr 0))
        oldy (length arr)
        newx (* 5 oldx)
        newy (* 5 oldy)]

    (def newarr (array/new newx))
    (loop [i :range [0 newx]]
      (set (newarr i) @[]))

    (loop [x :range [0 newx]]
      (loop [y :range [0 newy]]
        (let [xr (mod x oldx)
              yr (mod y oldy)
              oldv (get-in arr [xr yr])
              mult (+ (math/floor (/ y oldy)) (math/floor (/ x oldx)))]
          (def newv (+ (mod (+ oldv (dec mult)) 9) 1))
          #(pp [x y xr yr oldv newv mult])
          (aoc/set-in newarr [x y] newv))))

    newarr))

(defn getneighbors [arr [x y]]
  (let [maxx (length (get arr 0))
        maxy (length arr)]
    (->>
      [[(dec x) y] [x (dec y)] [x (inc y)] [(inc x) y]]
      (filter (fn [[x y]] (and (>= x 0) (< x maxx) (>= y 0) (< y maxy)))))))



(defn dijkstra [edges]
  (let [weights (aoc/map2d (fn [_] math/inf) edges)]
    # init the weights field
    (aoc/set-in weights [0 0] 0)

    (loop [i :range [0 10]]
      (loop [curx :range [0 (length (get weights 0))]]
        (loop [cury :range [0 (length weights)]]
          (let [curweight (get-in weights [cury curx])]
            (->>
              (getneighbors weights [curx cury])
              (map (fn [[x y]] [x y (get-in edges [y x])]))
              (map (fn [[x y risk]]
                    (->>
                      (get-in weights [y x] math/inf)      # get the current val at x/y or inf
                      (min (+ curweight risk))             # take the min between that and curnode + risk
                      (aoc/set-in weights [y x])))))))))   # and update it
    weights))


# part 1
(prin "part 1: ")
(let [finalweights (dijkstra (plines lines))]
  (pp (get-in finalweights [(dec (length finalweights)) (dec (length (get finalweights 0)))])))

# part 2
(prin "part 2: ")
(let [finalweights (dijkstra (expandarr (plines lines)))]
  (pp (get-in finalweights [(dec (length finalweights)) (dec (length (get finalweights 0)))])))
