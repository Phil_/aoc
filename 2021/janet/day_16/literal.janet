(def chars (string/trim (slurp "2021/day_16/input.txt")))

(defn hexToBit [hexchar]
  (case hexchar
    "0" "0000"
    "1" "0001"
    "2" "0010"
    "3" "0011"
    "4" "0100"
    "5" "0101"
    "6" "0110"
    "7" "0111"
    "8" "1000"
    "9" "1001"
    "A" "1010"
    "B" "1011"
    "C" "1100"
    "D" "1101"
    "E" "1110"
    "F" "1111"))

(defn bits [chars] (->>
  chars
  string/bytes
  (map string/from-bytes)
  (map hexToBit)
  string/join))

(defn arr2numbin [arr]
  (first (reduce 
    (fn [[accum ind] el]
      [(+ accum (* el (math/exp2 ind))) (dec ind)])
    [0 (dec (length arr))]
    arr)))

(defn strtodec [str]
  (->>
    (string/bytes str)
    (map string/from-bytes)
    (map scan-number)
    arr2numbin))

(defn parsenum [content]
  (var stop 0)
  (def content (string/join ["10000" content]))
  [
   (->>
     (range 0 (length content) 5)
     (take-until |(= (get content $) 48))
     (|(flatten [$ (+ 5 (last $))]))
     ((fn [foo] (set stop (+ 5 (last foo))) foo))
     (map |[$ (+ $ 5)])
     (map (fn [[start end]] (string/slice content (inc start) end)))
     string/join
     strtodec)
   (string/slice content stop -1)])

(defn parsePackage [bits]
  (if (> (length bits) 6)
    (let [version (strtodec (string/slice bits 0 3))
          typeid (strtodec (string/slice bits 3 6))
          content (string/slice bits 6 -1)]

      (match typeid
        4 (do
            (def [val retbits] (parsenum content))
            [val retbits])

        _ (let [lengthid (strtodec (string/slice content 0 1))
                rest (string/slice content 1 -1)
                [op startval] (match typeid
                     0 [+ 0]
                     1 [* 1]
                     2 [min math/inf]
                     3 [max math/-inf]
                     5 [(fn [x y] (if (> x y) 1 0)) nil]
                     6 [(fn [x y] (if (< x y) 1 0)) nil]
                     7 [(fn [x y] (if (= x y) 1 0)) nil]
                     )]
            (match lengthid
              0 (let [nbitsubp (strtodec (string/slice rest 0 15))
                      subps (string/slice rest 15 -1)]
                  (def origbits subps)
                  (var retbits subps)
                  (var val startval)
                  (var tmp @[])

                  (while (< (- (length origbits) (length retbits)) nbitsubp)
                    (def [rval leftbits] (parsePackage retbits))
                    (array/push tmp rval)
                    (set retbits leftbits))

                  [~(,op ,;tmp) retbits])

              1 (let [nsubp (strtodec (string/slice rest 0 11))
                      subps (string/slice rest 11 -1)]
                  (var retbits subps)
                  (var val startval)
                  (var tmp @[])

                  # parse nsubps packages consecutively, always returning the rest of the bits
                  (loop [i :range [0 nsubp]] 
                    (def [rval leftbits] (parsePackage retbits))
                    (array/push tmp rval)
                    (set retbits leftbits))

                  [~(,op ,;tmp) retbits])))))))


(->>
  # "C200B40A82"
  # "04005AC33890"
  "620080001611562C8802118E34"
  # "880086C3E88112"
  # "8A004A801A8002F478"
  #"9C005AC2F8F0"
  # "9C0141080250320F1802104A08"
  # "A0016C880162017C3686B18A3D4780"
  # "C0015000016115A2E0802F182340"
  # "CE00C43D881120"
  # "D8005AC2A8F0"
  #"F600BC2D8F"
  #chars
  bits
  parsePackage
  first
  pp)
