# ## --- Day 8: Seven Segment Search ---
# 
# You barely reach the safety of the cave when the whale smashes into the
# cave mouth, collapsing it. Sensors indicate another exit to this cave at
# a much greater depth, so you have no choice but to press on.
# 
# As your submarine slowly makes its way through the cave system, you
# notice that the four-digit [seven-segment
# displays](https://en.wikipedia.org/wiki/Seven-segment_display) in your
# submarine are malfunctioning; [they must have been
# damaged]{title="Yes, just the four-digit seven-segment ones. Whole batch must have been faulty."}
# during the escape. You'll be in a lot of trouble without them, so
# you'd better figure out what's wrong.
# 
# Each digit of a seven-segment display is rendered by turning on or off
# any of seven segments named `a` through `g`:
# 
#       0:      1:      2:      3:      4:
#      aaaa    ....    aaaa    aaaa    ....
#     b    c  .    c  .    c  .    c  b    c
#     b    c  .    c  .    c  .    c  b    c
#      ....    ....    dddd    dddd    dddd
#     e    f  .    f  e    .  .    f  .    f
#     e    f  .    f  e    .  .    f  .    f
#      gggg    ....    gggg    gggg    ....
# 
#       5:      6:      7:      8:      9:
#      aaaa    aaaa    aaaa    aaaa    aaaa
#     b    .  b    .  .    c  b    c  b    c
#     b    .  b    .  .    c  b    c  b    c
#      dddd    dddd    ....    dddd    dddd
#     .    f  e    f  .    f  e    f  .    f
#     .    f  e    f  .    f  e    f  .    f
#      gggg    gggg    ....    gggg    gggg
# 
# So, to render a `1`, only segments `c` and `f` would be turned on; the
# rest would be off. To render a `7`, only segments `a`, `c`, and `f`
# would be turned on.
# 
# The problem is that the signals which control the segments have been
# mixed up on each display. The submarine is still trying to display
# numbers by producing output on signal wires `a` through `g`, but those
# wires are connected to segments *randomly*. Worse, the wire/segment
# connections are mixed up separately for each four-digit display! (All of
# the digits *within* a display use the same connections, though.)
# 
# So, you might know that only signal wires `b` and `g` are turned on, but
# that doesn't mean *segments* `b` and `g` are turned on: the only digit
# that uses two segments is `1`, so it must mean segments `c` and `f` are
# meant to be on. With just that information, you still can't tell which
# wire (`b`/`g`) goes to which segment (`c`/`f`). For that, you'll need
# to collect more information.
# 
# For each display, you watch the changing signals for a while, make a
# note of *all ten unique signal patterns* you see, and then write down a
# single *four digit output value* (your puzzle input). Using the signal
# patterns, you should be able to work out which pattern corresponds to
# which digit.
# 
# For example, here is what you might see in a single entry in your notes:
# 
#     acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab |
#     cdfeb fcadb cdfeb cdbaf
# 
# (The entry is wrapped here to two lines so it fits; in your notes, it
# will all be on a single line.)
# 
# Each entry consists of ten *unique signal patterns*, a `|` delimiter,
# and finally the *four digit output value*. Within an entry, the same
# wire/segment connections are used (but you don't know what the
# connections actually are). The unique signal patterns correspond to the
# ten different ways the submarine tries to render a digit using the
# current wire/segment connections. Because `7` is the only digit that
# uses three segments, `dab` in the above example means that to render a
# `7`, signal lines `d`, `a`, and `b` are on. Because `4` is the only
# digit that uses four segments, `eafb` means that to render a `4`, signal
# lines `e`, `a`, `f`, and `b` are on.
# 
# Using this information, you should be able to work out which combination
# of signal wires corresponds to each of the ten digits. Then, you can
# decode the four digit output value. Unfortunately, in the above example,
# all of the digits in the output value (`cdfeb fcadb cdfeb cdbaf`) use
# five segments and are more difficult to deduce.
# 
# For now, *focus on the easy digits*. Consider this larger example:
# 
#     be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb |
#     fdgacbe cefdb cefbgd gcbe
#     edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec |
#     fcgedb cgb dgebacf gc
#     fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef |
#     cg cg fdcagb cbg
#     fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega |
#     efabcd cedba gadfec cb
#     aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga |
#     gecf egdcabf bgf bfgea
#     fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf |
#     gebdcfa ecba ca fadegcb
#     dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf |
#     cefg dcbef fcge gbcadfe
#     bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd |
#     ed bcgafe cdgba cbgef
#     egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg |
#     gbdfcae bgc cg cgb
#     gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc |
#     fgae cfgab fg bagce
# 
# Because the digits `1`, `4`, `7`, and `8` each use a unique number of
# segments, you should be able to tell which combinations of signals
# correspond to those digits. Counting *only digits in the output values*
# (the part after `|` on each line), in the above example, there are `26`
# instances of digits that use a unique number of segments (highlighted
# above).
# 
# *In the output values, how many times do digits `1`, `4`, `7`, or `8`
# appear?*
# 

# filter for 2 | 4 | 3 | 7 -> 1 4 7 8

#(def tchars (let [f (file/open "2021/day_08/tinput.txt")] (file/read f :all)))
#(def tchars (string/slice tchars 0 (- (length tchars) 1)))
#(def tlines (string/split "\n" tchars))

(import aoc)
(setdyn :year 2021)
(setdyn :day 08)
(def lines (aoc/daylines))

(def line0 (get lines 0))

# number -> panels illuminated
# 0 -> 6
# 1 -> 2
# 2 -> 5
# 3 -> 5
# 4 -> 4
# 5 -> 5
# 6 -> 6
# 7 -> 3
# 8 -> 7
# 9 -> 6

(defn countp1 [line]
  (let [[left right] (->> line (string/split " | ") (map (fn [side] (string/split " " side))))]
    (->> right 
         (map length)
         (filter (fn [l] (or (= l 2) (= l 4) (= l 3) (= l 7))))
         length)))

#(->> tlines (map countp1) sum print)
#(->> lines (map countp1) sum print)

# --------------------------------------------------------------------

(defn arreq? [arr1 arr2]
  "elementwise comparison 2 arrays"
  (and 
    (= (length arr1) (length arr2))
    (->> (zipcoll arr1 arr2)
         pairs
         (map (fn [[a b]] (= a b)))
         (filter (fn [x] x))
         length
         (= (length arr1)))))


(defn in? [str char]
    (->> str
      (filter (fn [foo] (= foo char)))
      length
      (< 0)))


#   1111
#  2    3
#  2    3
#   4444
#  5    6
#  5    6
#   7777

(defn aprint [arr]
  (->> arr (map string) (interpose " ") string/join print))

(defn segtonum [segments]
  "decodes an array of 7-segment ids to a number according to the scheme above"
  (let [seg (sorted segments)]
    (if (arreq? seg [1 2 3 5 6 7]) 0
      (if (arreq? seg [3 6]) 1
        (if (arreq? seg [1 3 4 5 7]) 2
          (if (arreq? seg [1 3 4 6 7]) 3
            (if (arreq? seg [2 3 4 6]) 4
              (if (arreq? seg [1 2 4 6 7]) 5
                (if (arreq? seg [1 2 4 5 6 7]) 6
                  (if (arreq? seg [1 3 6]) 7
                    (if (arreq? seg [1 2 3 4 5 6 7]) 8 9)))))))))))


(defn decodep2 [line]
  (let [[left right] (->> line (string/split " | ") (map (fn [side] (string/split " " side))))]
    # contsruct translation table from frequency analysis
    (def char2segtable (->>
      left
      string/join
      frequencies
      pairs
      (map (fn [[key val]]
        (if (= val 6) [key 2]
          (if (= val 4) [key 5]
            (if (= val 9) [key 6]
              (if (= val 7)
                (if (in? ((filter (fn [item] (= (length item) 4)) left) 0) key) [key 4] [key 7])
                (if (= val 8)
                  (if (in? ((filter (fn [item] (= (length item) 2)) left) 0) key) [key 3] [key 1]))))))))
      flatten
      ((fn [arr] (table ;arr)))))

    # translate every segment on the right hand to an array of numbers
    (def numarr (->>
      right 
      (map (fn [seg]
        (map 
          (fn [panel] (get char2segtable panel))
          seg)))
      (map segtonum)))
    
    # sum them all up to a number and return
    (->>
      numarr
      (map string)
      string/join
      scan-number)))


(def ttline "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf")
# apply function of part 2 to every line
(->>
  lines
  (map decodep2)
  sum
  print)
