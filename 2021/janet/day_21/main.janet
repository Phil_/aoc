# ## --- Day 21: Dirac Dice ---
# 
# There's not much to do as you slowly descend to the bottom of the
# ocean. The submarine computer [challenges you to a nice
# game]{title="A STRANGE GAME."} of *Dirac Dice*.
# 
# This game consists of a single
# [die](https://en.wikipedia.org/wiki/Dice), two
# [pawns](https://en.wikipedia.org/wiki/Glossary_of_board_games#piece),
# and a game board with a circular track containing ten spaces marked `1`
# through `10` clockwise. Each player's *starting space* is chosen
# randomly (your puzzle input). Player 1 goes first.
# 
# Players take turns moving. On each player's turn, the player rolls the
# die *three times* and adds up the results. Then, the player moves their
# pawn that many times *forward* around the track (that is, moving
# clockwise on spaces in order of increasing value, wrapping back around
# to `1` after `10`). So, if a player is on space `7` and they roll `2`,
# `2`, and `1`, they would move forward 5 times, to spaces `8`, `9`, `10`,
# `1`, and finally stopping on `2`.
# 
# After each player moves, they increase their *score* by the value of the
# space their pawn stopped on. Players' scores start at `0`. So, if the
# first player starts on space `7` and rolls a total of `5`, they would
# stop on space `2` and add `2` to their score (for a total score of `2`).
# The game immediately ends as a win for any player whose score reaches
# *at least `1000`*.
# 
# Since the first game is a practice game, the submarine opens a
# compartment labeled *deterministic dice* and a 100-sided die falls out.
# This die always rolls `1` first, then `2`, then `3`, and so on up to
# `100`, after which it starts over at `1` again. Play using this die.
# 
# For example, given these starting positions:
# 
#     Player 1 starting position: 4
#     Player 2 starting position: 8
# 
# This is how the game would go:
# 
# -   Player 1 rolls `1`+`2`+`3` and moves to space `10` for a total score
#     of `10`.
# -   Player 2 rolls `4`+`5`+`6` and moves to space `3` for a total score
#     of `3`.
# -   Player 1 rolls `7`+`8`+`9` and moves to space `4` for a total score
#     of `14`.
# -   Player 2 rolls `10`+`11`+`12` and moves to space `6` for a total
#     score of `9`.
# -   Player 1 rolls `13`+`14`+`15` and moves to space `6` for a total
#     score of `20`.
# -   Player 2 rolls `16`+`17`+`18` and moves to space `7` for a total
#     score of `16`.
# -   Player 1 rolls `19`+`20`+`21` and moves to space `6` for a total
#     score of `26`.
# -   Player 2 rolls `22`+`23`+`24` and moves to space `6` for a total
#     score of `22`.
# 
# ...after many turns...
# 
# -   Player 2 rolls `82`+`83`+`84` and moves to space `6` for a total
#     score of `742`.
# -   Player 1 rolls `85`+`86`+`87` and moves to space `4` for a total
#     score of `990`.
# -   Player 2 rolls `88`+`89`+`90` and moves to space `3` for a total
#     score of `745`.
# -   Player 1 rolls `91`+`92`+`93` and moves to space `10` for a final
#     score, `1000`.
# 
# Since player 1 has at least `1000` points, player 1 wins and the game
# ends. At this point, the losing player had `745` points and the die had
# been rolled a total of `993` times; `745 * 993 = 739785`.
# 
# Play a practice game using the deterministic 100-sided die. The moment
# either player wins, *what do you get if you multiply the score of the
# losing player by the number of times the die was rolled during the
# game?*

(import aoc)

# part 1

(defn warparound [n incr]
  (inc (mod (dec (+ n incr)) 10)))

(defn players [p1start p2start]
  (var step 1)
  (var p1points 0)
  (var p2points 0)
  (var p1pos p1start)
  (var p2pos p2start)

  (generate [_ :iterate true]
    (def ndth (* 3 step))
    (def stepinc (* 3 (dec ndth)))
    (set step (inc step))

    (match (inc (mod step 2))
      1 (do
          (set p1pos (inc (mod (dec (+ p1pos stepinc)) 10)))
          (set p1points (+ p1points p1pos))
          [:p1 p1points (* 3 step)])
      2 (do
          (set p2pos (inc (mod (dec (+ p2pos stepinc)) 10)))
          (set p2points (+ p2points p2pos))
          [:p2 p2points (* 3 step)]))))

(prin "part 1: ")
(->>
  (players 10 9)
  (take-until (fn [[_ score]] (> score 1000)))
  last
  ((fn [[_ x y]] (* x y)))
  pp)

# part 2

(print "part 2: ")

(defn gethist []
  (def ret @{})
  (for i 1 4
    (for j 1 4
      (for k 1 4
        (set 
          (ret (+ i j k)) (inc (get ret (+ i j k) 0))))))
  ret)


(defn wstep [pos incr]
  (inc (mod (dec (+ pos incr)) 10)))

(def hist (gethist))

# TODO fix
(defn part2 [p1pos p2pos &opt p1score p2score]
  (default p1score 0)
  (default p2score 0)
  (def scorelimit 21)

  (if (or (>= p1score scorelimit) (>= p2score scorelimit))
    (if (>= p1score scorelimit) [1 0] [0 1])
    (->>
      (pairs hist)
      (map 
        (fn [[incr occ]]
          (def p1posnew (wstep p1pos incr))
          (def p1scorenew (+ p1score p1posnew))
          (def [p2w p1w] (part2 p2pos p1posnew p2score p1scorenew))
          [(* p1w occ) (* p2w occ)]))
      (reduce2 (fn [[tx ty] [x y]] [(+ tx x) (+ ty y)])))))

(pp (part2 4 8))
