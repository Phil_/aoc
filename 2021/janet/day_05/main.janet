# ## --- Day 5: Hydrothermal Venture ---
# 
# You come across a field of [hydrothermal
# vents](https://en.wikipedia.org/wiki/Hydrothermal_vent) on the ocean
# floor! These vents constantly produce large, opaque clouds, so it would
# be best to avoid them if possible.
# 
# They tend to form in *lines*; the submarine helpfully produces a list of
# nearby [lines of vents]{title="Maybe they're Bresenham vents."} (your
# puzzle input) for you to review. For example:
# 
#     0,9 -> 5,9
#     8,0 -> 0,8
#     9,4 -> 3,4
#     2,2 -> 2,1
#     7,0 -> 7,4
#     6,4 -> 2,0
#     0,9 -> 2,9
#     3,4 -> 1,4
#     0,0 -> 8,8
#     5,5 -> 8,2
# 
# Each line of vents is given as a line segment in the format
# `x1,y1 -> x2,y2` where `x1`,`y1` are the coordinates of one end the line
# segment and `x2`,`y2` are the coordinates of the other end. These line
# segments include the points at both ends. In other words:
# 
# -   An entry like `1,1 -> 1,3` covers points `1,1`, `1,2`, and `1,3`.
# -   An entry like `9,7 -> 7,7` covers points `9,7`, `8,7`, and `7,7`.
# 
# For now, *only consider horizontal and vertical lines*: lines where
# either `x1 = x2` or `y1 = y2`.
# 
# So, the horizontal and vertical lines from the above list would produce
# the following diagram:
# 
#     .......1..
#     ..1....1..
#     ..1....1..
#     .......1..
#     .112111211
#     ..........
#     ..........
#     ..........
#     ..........
#     222111....
# 
# In this diagram, the top left corner is `0,0` and the bottom right
# corner is `9,9`. Each position is shown as *the number of lines which
# cover that point* or `.` if no line covers that point. The top-left pair
# of `1`s, for example, comes from `2,2 -> 2,1`; the very bottom row is
# formed by the overlapping lines `0,9 -> 5,9` and `0,9 -> 2,9`.
# 
# To avoid the most dangerous areas, you need to determine *the number of
# points where at least two lines overlap*. In the above example, this is
# anywhere in the diagram with a `2` or larger - a total of `5` points.
# 
# Consider only horizontal and vertical lines. *At how many points do at
# least two lines overlap?*
# 

(import aoc)
(setdyn :year 2021)
(setdyn :day 5)
(def lines (aoc/daylines))

(defn parseline [line] 
  (let [
        [from to] (string/split " -> " line)
        ppart (fn [x] (map scan-number (string/split "," x)))]
    [;(ppart from) ;(ppart to)]))

(defn getl [[x1 y1 x2 y2]]
  #(print "from (" x1 ", " y1 ") to (" x2 ", " y2 ")")
  (if (or (= x1 x2) (= y1 y2))
    (if (= x1 x2)
      # straight up/down
      (map (fn [y] [x1 y]) (range (min y1 y2) (+ 1 (max y1 y2))))
      # straight left/right
      (map (fn [x] [x y1]) (range (min x1 x2) (+ 1 (max x1 x2)))))

    # non-straight (45 degrees)
    (if (or 
          (and (< x1 x2) (< y1 y2))
          (and (> x1 x2) (> y1 y2)))

      # bl -> tr or tr -> bl
      (map (fn [step] [(+ step (min x1 x2)) (+ step (min y1 y2))])
           (range 0 (+ 1 (- (max x1 x2) (min x1 x2)))))

      # br -> tl or tl -> br
      (map (fn [step] [(+ step (min x1 x2)) (- (max y1 y2) step)])
           (range 0 (+ 1 (- (max x1 x2) (min x1 x2))))))))

(defn draw_volc [overlaps line]
  (let [t (table/clone overlaps)]
    (map
      (fn [[x y]]
        #(print "x: " x ", y: " y)
        (set (t [x y]) (+ 1 (get t [x y] 0))))
      (getl line))
    t))

(def testlines (string/split "\n" "0,9 -> 5,9\n
8,0 -> 0,8\n
9,4 -> 3,4\n
2,2 -> 2,1\n
7,0 -> 7,4\n
6,4 -> 2,0\n
0,9 -> 2,9\n
3,4 -> 1,4\n
0,0 -> 8,8\n
5,5 -> 8,2"))

(def plines (map parseline lines))
(def tlines (map parseline testlines))

(def p1lines (filter (fn [[x1 y1 x2 y2]] (or (= x1 x2) (= y1 y2))) plines))
(def p1tlines (filter (fn [[x1 y1 x2 y2]] (or (= x1 x2) (= y1 y2))) tlines))

(def overlaps (reduce draw_volc @{} plines))
(print (length (filter (fn [x] (> x 1)) (values overlaps))))
