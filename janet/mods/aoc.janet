(defn- day []
  (let [int (min 25 (max 1 (dyn :day 1)))]
    (if (< int 10)
      (string/join ["0" (string int)])
      (string int))))


(defn- year []
  (let [int (min 2023 (max 2015 (dyn :year 2015)))]
    (string int)))


(defn dayfile [] (string/join [ (year) "/inputs/day_" (day) ".txt"]))
(defn daychars [] (string/trim (slurp (dayfile))))
(defn daylines [] (string/split "\n" (daychars)))


(defn singlechars [line] (->> line string/bytes (map string/from-bytes)))


(defn set-in [ind ks newval &opt origind]
  (default origind ind)
  (match (length ks)
    1 (do (set (ind (first ks)) newval) origind)
    _ (set-in (get ind (first ks)) (array/slice ks 1 -1) newval origind)))


(defn map2d [f arr] (map |(map f $) arr))


(defn xor [a b]
  (and (or a b) (or (not a) (not b))))


(defn deepcopy [arr]
  (match (type arr)
    :number arr
    :string arr
    _ (array ;(map deepcopy arr))))


(defn neigh4 [x y]
  [[(dec x) y] [x (dec y)]
   [x (inc y)] [(inc x) y]])


(defn neigh8 [x y]
  [[(dec x) (dec y)] [(dec x) y]
   [(dec x) (inc y)] [x (dec y)]
   [x (inc y)] [(inc x) (dec y)]
   [(inc x) y] [(inc x) (inc y)]])


(defn in? [arr val]
  (match (type val)
    :array (->> arr (count (fn [x] (= x (tuple ;val)))) (< 0))
    _ (->> arr (count (fn [x] (= x val))) (< 0))))


(defn unique [list]
  (let [n (array/new (length list))]
    (do 
      (each el list (if (not (in? n el)) (array/push n el)))
      n)))


(defn chunks [n list]
  (let [len (length list) n-lists (/ len n)]
    (->>
      (range 0 n-lists)
      (map (fn [i] (array/slice list (* n i) (* n (+ i 1))))))))


(defn transpose [arr]
  (map
    (fn [idx] (map (fn [row] (get row idx)) arr))
    (range 0 (length (get arr 0)))))


(defn enumerate [arr1] (map |[$ (get arr1 $)] (range 0 (length arr1))))


(defn zip [arr1 arr2]
  (map
    |[(get arr1 $) (get arr2 $)]
    (range 0 (min (length arr1) (length arr2)))))


(defn windows [n arr]
  (->>
    (length arr) (- n) math/abs inc
    (range 0)
    (map
      |(map
         |(get arr $)
         (range $ (+ $ n))))))


(defmacro dbg [x]
  (let [sym (string (if (= (type x) :symbol) (string x) (type x)) ": ")]
    ~(do
      (prin ,sym)
      (pp ,x))))


(defn dist [v1 v2] (sum (map (fn [[e1 e2]] (math/pow (math/abs (- e2 e1)) 2)) (zip v1 v2))))


(defn draw-arr- [transfn arr]
  (let [trarr (map2d transfn arr)
        draw-len (fn [val]
                   (match (type val)
                     :number (let [base (math/floor (inc (math/log10 val)))] (if (< 0 val) (inc base) base))
                     :string (inc (length val))))
        cellwidth (max-of (map draw-len (flatten arr)))
        _ (pp cellwidth)]
    (->> trarr
        (map2d |(string/format (string/join ["%0" (string cellwidth) "s"]) $))
        (map string/join)
        (map print))))


(defn draw-arr [arr]
  (draw-arr- string arr))


(defn draw-arr-pred [pred arr]
  (draw-arr- |(if (pred $) "█" " ") arr))


(defn draw-coords [coord-list & extra-args]
  (let [[mx my] (get extra-args 0 [0 0])
        [llx lly] (get extra-args 1 [0 0])
        min-x (min-of (map |(get $ 0) coord-list))
        max-x (max-of (map |(get $ 0) coord-list))
        min-y (min-of (map |(get $ 1) coord-list))
        max-y (max-of (map |(get $ 1) coord-list))
        dx (max mx (inc (- max-x min-x)))
        dy (max my (inc (- max-y min-y)))
        arr (map (fn [_] (array/new-filled dx false)) (array/new-filled dy nil))]
    # TODO: draw coordinate grid
    #(print (string/join (interpose " " (map string [dx dy]))))
    #(array/concat coord-list @[[0 0]])
    (each [x y] coord-list (set-in arr [(- y min-y) (- x min-x)] true))
    (draw-arr-pred identity arr))
  nil)


(defn draw-coord-values [coord-value-list & extra-args]
  (let [[mx my] (get extra-args 0 [0 0])
        [llx lly] (get extra-args 1 [0 0])
        min-x (min-of (map |(get $ 0) coord-value-list))
        max-x (max-of (map |(get $ 0) coord-value-list))
        min-y (min-of (map |(get $ 1) coord-value-list))
        max-y (max-of (map |(get $ 1) coord-value-list))
        dx (max mx (inc (- max-x min-x)))
        dy (max my (inc (- max-y min-y)))
        arr (map (fn [_] (array/new-filled dy nil)) (array/new-filled dx nil))]
    # TODO: draw coordinate grid
    #(print (string/join (interpose " " (map string [dx dy]))))
    #(array/concat coord-value-list @[[0 0 0]])
    (each [x y val] coord-value-list (set-in arr [(- y (min min-y lly)) (- x (min min-x llx))] val))
    (draw-arr- (fn [e] (if (= nil e) "_" (string e))) arr))
  nil)
