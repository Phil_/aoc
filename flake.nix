# TODO: get second part of each day with pandoc
# TODO: rewrite aoc script in other language (bash kinda meh)

{
  description = "Advent of Code Flake";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    janet-lang-src = {
      url = "github:janet-lang/janet";
      flake = false;
    };
  };

  outputs = { self, flake-utils, ... }@inputs:
  flake-utils.lib.eachDefaultSystem (system: let
    util = import ./nix { inherit system inputs; };
  in {
    devShells.default = util.mkShell util.languages.janet;
    checks = builtins.mapAttrs (_: util.mkShell) util.languages;
  });
}
