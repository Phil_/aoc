# ## --- Day 12: Hill Climbing Algorithm ---
# 
# You try contacting the Elves using your [handheld
# device]{title="When you look up the specs for your handheld device, every field just says "plot"."},
# but the river you're following must be too low to get a decent signal.
# 
# You ask the device for a heightmap of the surrounding area (your puzzle
# input). The heightmap shows the local area from above broken into a
# grid; the elevation of each square of the grid is given by a single
# lowercase letter, where `a` is the lowest elevation, `b` is the
# next-lowest, and so on up to the highest elevation, `z`.
# 
# Also included on the heightmap are marks for your current position (`S`)
# and the location that should get the best signal (`E`). Your current
# position (`S`) has elevation `a`, and the location that should get the
# best signal (`E`) has elevation `z`.
# 
# You'd like to reach `E`, but to save energy, you should do it in *as
# few steps as possible*. During each step, you can move exactly one
# square up, down, left, or right. To avoid needing to get out your
# climbing gear, the elevation of the destination square can be *at most
# one higher* than the elevation of your current square; that is, if your
# current elevation is `m`, you could step to elevation `n`, but not to
# elevation `o`. (This also means that the elevation of the destination
# square can be much lower than the elevation of your current square.)
# 
# For example:
# 
#     Sabqponm
#     abcryxxl
#     accszExk
#     acctuvwj
#     abdefghi
# 
# Here, you start in the top-left corner; your goal is near the middle.
# You could start by moving down or right, but eventually you'll need to
# head toward the `e` at the bottom. From there, you can spiral around to
# the goal:
# 
#     v..v<<<<
#     >v.vv<<^
#     .>vv>E^^
#     ..v>>>^^
#     ..>>>>>^
# 
# In the above diagram, the symbols indicate whether the path exits each
# square moving up (`^`), down (`v`), left (`<`), or right (`>`). The
# location that should get the best signal is still `E`, and `.` marks
# unvisited squares.
# 
# This path reaches the goal in `31` steps, the fewest possible.
# 
# *What is the fewest steps required to move from your current position to
# the location that should get the best signal?*
# 
(import aoc)
(setdyn :year 2022)
(setdyn :day 12)
(def [tlines lines] (map |(string/split "\n" $) (string/split "\n\n" (aoc/daychars))))


(defn find-start-end [arr]
  [(first (first (filter |(= (last $) -14) arr)))
   (first (first (filter |(= (last $) -28) arr)))])


(defn primitive-dijkstra [source target arr]
  # construct the queue
  (let [q (array/concat (map |@{:loc (first $) :val (last $) :dist math/inf :prev nil} arr))
        _ (set ((first (filter |(= (get $ :loc) source) q)) :dist) 0)
        _ (set ((first (filter |(= (get $ :loc) source) q)) :val) 0)
        _ (set ((first (filter |(= (get $ :loc) target) q)) :val) (max-of (map |(get $ :val) q)))
        done-arr @[]
        distfn (fn [from to] (let [dist (- (get to :val) (get from :val))] (if (<= dist 1) dist math/inf)))]
    (while (> (length q) 0)
      # iterate over the q, taking the element with the smallest distance
      (let [u (array/pop (reverse! (sort-by |(get $ :dist) q)))
            # find the neighbor with the smallest distance
            neigh-coords (aoc/neigh4 ;(get u :loc))
            neighs (filter |(aoc/in? neigh-coords (get $ :loc)) q)]
        (each neigh neighs
          # set each neighbor's distance and prev
          (let [newdist (+ (get u :dist) (distfn u neigh))]
            (if (< newdist (get neigh :dist))
              (do
                (set (neigh :dist) newdist)
                (set (neigh :prev) (get u :loc))))))
        (array/concat done-arr u)
        (if (= (get u :loc) target) (break))))
    done-arr))

(defn find-path [arr]
  (let [[start end] (find-start-end arr)]
    [(primitive-dijkstra start end arr) start end]))


(def arr (->> tlines
             (map string/bytes)
             (aoc/map2d (fn [b] (- b 97)))
             (aoc/transpose)
             (map aoc/enumerate)
             (aoc/transpose)
             (map aoc/enumerate)
             (aoc/map2d (fn [[x [y v]]] [[x y] v]))))
(def [path start end] (find-path (reduce2 array/concat arr)))

  #(->> path
    #(filter (fn [node] (not= (get node :dist) math/inf)))
    #(map |[;(get $ :loc) (get $ :dist)])
    #(aoc/draw-coord-values))

(defn pos2node [pos] (first (filter |(= pos (get $ :loc)) path)))

(var curnode (pos2node end))
(var len_path 0)
(var path @[])
(while (not= (get curnode :loc) start)
  (do
    (pp curnode)
    (array/concat path curnode)
    (set curnode (pos2node (get curnode :prev)))
    (set len_path (inc len_path))))

(pp len_path)



(aoc/draw-coords (map |(get $ :loc) path))
