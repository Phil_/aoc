# ## --- Day 5: Supply Stacks ---
# 
# The expedition can depart as soon as the final supplies have been
# unloaded from the ships. Supplies are stored in stacks of marked
# *crates*, but because the needed supplies are buried under many other
# crates, the crates need to be rearranged.
# 
# The ship has a *giant cargo crane* capable of moving crates between
# stacks. To ensure none of the crates get crushed or fall over, the crane
# operator will rearrange them in a series of carefully-planned steps.
# After the crates are rearranged, the desired crates will be at the top
# of each stack.
# 
# The Elves don't want to interrupt the crane operator during this
# delicate procedure, but they forgot to ask her *which* crate will end up
# where, and they want to be ready to unload them as soon as possible so
# they can embark.
# 
# They do, however, have a drawing of the starting stacks of crates *and*
# the rearrangement procedure (your puzzle input). For example:
# 
#         [D]    
#     [N] [C]    
#     [Z] [M] [P]
#      1   2   3 
# 
#     move 1 from 2 to 1
#     move 3 from 1 to 3
#     move 2 from 2 to 1
#     move 1 from 1 to 2
# 
# In this example, there are three stacks of crates. Stack 1 contains two
# crates: crate `Z` is on the bottom, and crate `N` is on top. Stack 2
# contains three crates; from bottom to top, they are crates `M`, `C`, and
# `D`. Finally, stack 3 contains a single crate, `P`.
# 
# Then, the rearrangement procedure is given. In each step of the
# procedure, a quantity of crates is moved from one stack to a different
# stack. In the first step of the above rearrangement procedure, one crate
# is moved from stack 2 to stack 1, resulting in this configuration:
# 
#     [D]        
#     [N] [C]    
#     [Z] [M] [P]
#      1   2   3 
# 
# In the second step, three crates are moved from stack 1 to stack 3.
# Crates are moved *one at a time*, so the first crate to be moved (`D`)
# ends up below the second and third crates:
# 
#             [Z]
#             [N]
#         [C] [D]
#         [M] [P]
#      1   2   3
# 
# Then, both crates are moved from stack 2 to stack 1. Again, because
# crates are moved *one at a time*, crate `C` ends up below crate `M`:
# 
#             [Z]
#             [N]
#     [M]     [D]
#     [C]     [P]
#      1   2   3
# 
# Finally, one crate is moved from stack 1 to stack 2:
# 
#             [Z]
#             [N]
#             [D]
#     [C] [M] [P]
#      1   2   3
# 
# The Elves just need to know *which crate will end up on top of each
# stack*; in this example, the top crates are `C` in stack 1, `M` in stack
# 2, and `Z` in stack 3, so you should combine these together and give the
# Elves the message `CMZ`.
# 
# *After the rearrangement procedure completes, what crate ends up on top
# of each stack?*
# 
(import aoc)
(setdyn :year 2022)
(setdyn :day 5)

(def chars (aoc/daychars))
(def parts (string/split "\n\n" chars))

(def [layout instructions]
  (->> (aoc/daychars)
       (string/split "\n\n")
       (map (partial string/split "\n"))))

(def stacks (->>
  layout
  (map |(string $ " "))
  (map string/bytes)
  (map (partial aoc/chunks 4))
  (aoc/map2d |(get $ 1))
  (aoc/map2d string/from-bytes)
  (aoc/transpose)
  (map reverse)
  (map (partial take-until |(= $ " ")))
  (map |(slice $ 1))
  (map |(array ;$))))

(def p-instrs
  (->> instructions
       (map (partial string/split " "))
       (map |[(get $ 1) (get $ 3) (get $ 5)])
       (aoc/map2d scan-number)))

(prin "part 1: ")
(->>
  p-instrs
  (reduce
    (fn [sx [n fro to]]
      (do
        (repeat n
          (do
            (array/push
              (get sx (- to 1))
              (array/pop (get sx (- fro 1))))))
        sx))
    (aoc/deepcopy stacks))
  (map last)
  (string/join)
  pp)

(prin "part 2: ")
(->>
  p-instrs
  (reduce
    (fn [sx [n fro to]]
      (do
        (let [tmp (array/new n)]
          (repeat n (array/push tmp (array/pop (get sx (- fro 1)))))
          (repeat n (array/push (get sx (- to 1)) (array/pop tmp))))
        sx))
    (aoc/deepcopy stacks))
  (map last)
  (string/join)
  pp)
