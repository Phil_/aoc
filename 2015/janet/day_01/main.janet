# ## --- Day 1: Not Quite Lisp ---
#
# Santa was hoping for a white Christmas, but his weather machine's
# "snow" function is powered by stars, and he's fresh out! To save
# Christmas, he needs you to collect *fifty stars* by December 25th.
#
# Collect stars by helping Santa solve puzzles. Two puzzles will be made
# available on each day in the Advent calendar; the second puzzle is
# unlocked when you complete the first. Each puzzle grants *one star*.
# [Good
# luck!]{title="Also, some puzzles contain Easter eggs like this one. Yes, I know it's not traditional to do Advent calendars for Easter."}
#
# Here's an easy puzzle to warm you up.
#
# Santa is trying to deliver presents in a large apartment building, but
# he can't find the right floor - the directions he got are a little
# confusing. He starts on the ground floor (floor `0`) and then follows
# the instructions one character at a time.
#
# An opening parenthesis, `(`, means he should go up one floor, and a
# closing parenthesis, `)`, means he should go down one floor.
#
# The apartment building is very tall, and the basement is very deep; he
# will never find the top or bottom floors.
#
# For example:
#
# -   `(())` and `()()` both result in floor `0`.
# -   `(((` and `(()(()(` both result in floor `3`.
# -   `))(((((` also results in floor `3`.
# -   `())` and `))(` both result in floor `-1` (the first basement
#     level).
# -   `)))` and `)())())` both result in floor `-3`.
#
# To *what floor* do the instructions take Santa?

(defn inst-to-int [char]
  # 40 "(" => 1
  # 41 ")" => -1
  (+ (* (- char 40) -2) 1))

(def lines (let [f (file/open "2015/day_01/input.txt")] (file/read f :all)))

# solution to 1
(def ints (map inst-to-int (string/slice lines 0 -2)))
(print (sum ints))

# ---------------------------------------

(defn cumsum [arr]
  (map 
    (fn [i] 
      (sum (array/slice arr 0 (+ i 1))))
    (range (length arr))))

(def cint (cumsum ints))
(print (0 (map (fn [x] (+ x 1))
  (filter
    (fn [x] (= (get cint x) -1))
    (range (length cint))))))
