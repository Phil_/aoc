; ## --- Day 6: Probably a Fire Hazard ---
; 
; Because your neighbors keep defeating you in the holiday house
; decorating contest year after year, you've decided to deploy one
; million lights in a [1000x1000
; grid]{title="Hey, be glad I'm not asking for the resistance between two points!"}.
; 
; Furthermore, because you've been especially nice this year, Santa has
; mailed you instructions on how to display the ideal lighting
; configuration.
; 
; Lights in your grid are numbered from 0 to 999 in each direction; the
; lights at each corner are at `0,0`, `0,999`, `999,999`, and `999,0`. The
; instructions include whether to `turn on`, `turn off`, or `toggle`
; various inclusive ranges given as coordinate pairs. Each coordinate pair
; represents opposite corners of a rectangle, inclusive; a coordinate pair
; like `0,0 through 2,2` therefore refers to 9 lights in a 3x3 square. The
; lights all start turned off.
; 
; To defeat your neighbors this year, all you have to do is set up your
; lights by doing the instructions Santa sent you in order.
; 
; For example:
; 
; -   `turn on 0,0 through 999,999` would turn on (or leave on) every
;     light.
; -   `toggle 0,0 through 999,0` would toggle the first line of 1000
;     lights, turning off the ones that were on, and turning on the ones
;     that were off.
; -   `turn off 499,499 through 500,500` would turn off (or leave off) the
;     middle four lights.
; 
; After following the instructions, *how many lights are lit*?

(local util (require "aoc"))

(fn lset [lights x y val]
  (tset (. lights (+ 1 x)) (+ 1 y) val))

(fn lget [lights x y]
  (local res (. (. lights (+ x 1)) (+ y 1)))
  (if (= res nil) 0 res))

(fn handle [lights inst fromxy toxy]
  (let [(fromx fromy) (unpack (util.split fromxy ","))
        (tox toy) (unpack (util.split toxy ","))]
    (print inst fromxy toxy (* (- tox fromx) (- toy fromy)))
    (for [x fromx tox] (for [y fromy toy]
      (match inst
        "on" (lset lights x y 1)
        "off" (lset lights x y 0)
        "toggle" (lset lights x y (-  1 (lget lights x y))))))))

(fn handle_2 [lights inst fromxy toxy]
  (let [(fromx fromy) (unpack (util.split fromxy ","))
        (tox toy) (unpack (util.split toxy ","))
        llset (partial lset lights)
        llget (partial lget lights)]
    (print inst fromxy toxy (* (- tox fromx) (- toy fromy)))
    (for [x fromx tox] (for [y fromy toy]
      (local val (llget x y))
      (match inst
        "on" (llset x y (+ val 1))
        "off" (llset x y (if (> (- val 1) 0) (- val 1) 0))
        "toggle" (llset x y (+ val 2)))))))

(var lights [])
(for [x 1 1000]
  (tset lights x [0]))

(let [util (require "aoc") lines (util.readlines "./2015/day_06/input.txt")]
  (each [_ line (ipairs lines)]
    (let [(toxy _ fromxy inst) (unpack (pick-values 4 (util.reverse (util.split line " "))))]
      (handle_2 lights inst fromxy toxy))))

(local sum_lights (accumulate [total 0 _ rsum  (ipairs
    (icollect [_ row (pairs lights)]
      (accumulate [sum 0 _ val (pairs row)]
        (+ sum val))))]
  (+ total rsum)))

(print sum_lights)
