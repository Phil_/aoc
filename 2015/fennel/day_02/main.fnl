; I was told there Would be no Math

(var total_area 0)
(var total_ribbon 0)

(fn parse_line [line]
  "parses the line given, return l, w and h"
  (local ltrs [])
  (each [val (string.gmatch line "%d+")]
    (table.insert ltrs val))
  (values (. ltrs 1) (. ltrs 2) (. ltrs 3)))

(fn surface_area [l w h] 
  "calculates the surface area of a box"
  (let [side1 (* l w) side2 (* w h) side3 (* h l)]
    (+ (* side1 2) (* side2 2) (* side3 2) (math.min side1 side2 side3))))

(fn ribbon_area [l w h]
  "calculates the ribbon area for a given box"
  (let [tie (* l w h) wrap (math.min (* 2 (+ l w)) (* 2 (+ w h)) (* 2 (+ h l)))]
    (+ tie wrap)))

(each [line (io.lines :days/2/day2.input)]
  (let [(l w h) (parse_line line)]
    (set total_area (+ total_area (surface_area l w h)))
    (set total_ribbon (+ total_ribbon (ribbon_area l w h)))))

(print (ribbon_area 2 3 4))
(print (.. "Presents: " total_area))
(print (.. "Ribbon: " total_ribbon))
