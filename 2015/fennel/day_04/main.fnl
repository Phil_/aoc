; Day 4: The Ideal Stocking Stuffer

(fn valid? [input num_zeros]
  "checks if the given input starts with all zeros"
  (= (string.sub input 1 num_zeros) (string.rep "0" num_zeros)))

(fn solution [input num_zeros]
  (let [md5 (require :md5)]
    (var num_b 0)
    (while
      (not (valid? (md5.sumhexa (.. input num_b)) num_zeros))
      (set num_b (+ num_b 1)))
    (print num_b " | " (.. input num_b) "=>" (md5.sumhexa (.. input num_b)))))

(solution "abcdef" 5)
(solution "pqrstuv" 5)
(solution "bgvyzdsv" 5)
(solution "bgvyzdsv" 6)
