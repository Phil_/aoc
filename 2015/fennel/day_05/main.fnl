; Day 5: Doesn't He Have Intern-Elves For This?

(fn nice1? [input]
  "checks if the line is nice"
  (var nvow 0)
  (var twice 0)
  (var pure true)
  (var pchar "")
  (var nstrings ["ab" "cd" "pq" "xy"])
  (var vowels ["a" "e" "i" "o" "u"])

  (for [i 1 (string.len input) :until (not pure)]
    (let [char (string.sub input i i)]
      (let [sus (.. pchar char)]        ; check for purity
        (each [_ string (ipairs nstrings)]
          (if (= string sus) (set pure false))))

      (each [_ vowel (ipairs vowels)]   ; check for vowel
        (if (= char vowel)
          (set nvow (+ nvow 1))))

      (if (= char pchar) (set twice (+ twice 1)))
      (set pchar char)))
  (local nice (and (> nvow 2) (> twice 0) pure))
  (print nice nvow twice pure input)
  nice)

(fn nice2? [input]
  "checks if the line is nice"
  (var contp false)
  (var repeats 0)
  (var nice false)

  (for [i 1 (string.len input) 2]
    (let [at (fn [j] (string.sub input (+ i j) (+ i j)))]
      (if (and 
            (< (+ i 1) (string.len input))
            (or
              (= (at 0) (at 2))
              (= (at 1) (at 3))))
          (do 
            (set repeats (+ repeats 1))

            (print "\t" (if (= (at 0) (at 2))
              (string.sub input i (+ i 2))
              (string.sub input (+ i 1) (+ i 3)))))))

    (for [j (+ i 2) (string.len input)]
      (let [opair (string.sub input j (+ j 1))
            pair (string.sub input i (+ i 1))]
        (if (= pair opair)
          (do 
            (set contp true) 
            (print "\t" pair)))))

    (set nice (and contp (> repeats 0))))
  (print (if (= nice true) "nice" "naughty") contp repeats input)
  nice)

(local test_strings_1 [
  "ugknbfddgicrmopn"
  "aaa"
  "jchzalrnumimnmhp"
  "haegwjzuvuyypxyu"
  "dvszwmarrgswjxmb"
])

(local test_strings_2 [
  "xyxy"
  "aabcdefgaa"
  "aaa"
  "xyx"
  "abcdefeghi"
  "efe"
  "aaa"
  "qjhvhtzxzqqjkmpb"
  "xxyxx"
  "uurcxstgmygtbstg"
  "ieodomkazucvgmuy"
])

(local file_strings (icollect [line (io.lines :days/5/day5.input)] line))

(local nice_strings [])
(each [_ line (ipairs test_strings_2)]
  (if (nice2? line)
    (table.insert nice_strings line)))
(print (length nice_strings))
