; Day 1: Not Quite Lisp

(var floor 0)
(var print-first true)

(let [input (with-open [fin (io.open :2015/day_01/input.txt)] ((fin:lines)))]
  (for [i 1 (string.len input)]
    (let [char (string.sub input i i)]
      (if (= char (string.char 40))
             (set floor (+ floor 1))
             (do 
               (if (and (= floor 0) (= print-first true))
                 (do 
                   (set print-first false)
                   (print (.. "first basement:\t" i))))
               (set floor (- floor 1)))))))

(print (.. "final floor:\t" floor))
