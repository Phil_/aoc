; Day 3: Perfectly Spherical Houses in a Vacuum

; array of (x, y, visit_count) arrrays
(var coords [[0 0 2]])

; enable part 2
(var part2 true)

; current position
(var curx_man 0)
(var cury_man 0)
(var curx_robo 0)
(var cury_robo 0)


(fn visit_grid [x y]
  "checks if a given coordinate has been visited before"
  (var missing true)
  (each [index value (ipairs coords) :until (not missing)]
    (let [(vis_x vis_y vis_cnt) (unpack value)]
      (if (and (= vis_x x) (= vis_y y))
        (do 
          (set missing false)
          (tset coords index [vis_x vis_y (+ vis_cnt 1)])))))
  (if (= missing true)
    (table.insert coords [x y 1])))

(fn walk [input]
  (for [i 1 (string.len input)]
    (let [char (string.sub input i i)]
      (if (and (= 0 (% i 2)) (= part2 true))
        (do
          (if (= char "^")
            (do
              (set cury_man (+ cury_man 1))
              (visit_grid curx_man cury_man)))
          (if (= char "v")
            (do 
              (set cury_man (- cury_man 1))
              (visit_grid curx_man cury_man)))
          (if (= char "<")
            (do
              (set curx_man (- curx_man 1))
              (visit_grid curx_man cury_man)))
          (if (= char ">")
            (do
              (set curx_man (+ curx_man 1))
              (visit_grid curx_man cury_man))))
        (do
          (if (= char "^")
            (do
              (set cury_robo (+ cury_robo 1))
              (visit_grid curx_robo cury_robo)))
          (if (= char "v")
            (do 
              (set cury_robo (- cury_robo 1))
              (visit_grid curx_robo cury_robo)))
          (if (= char "<")
            (do
              (set curx_robo (- curx_robo 1))
              (visit_grid curx_robo cury_robo)))
          (if (= char ">")
            (do
              (set curx_robo (+ curx_robo 1))
              (visit_grid curx_robo cury_robo))))
          ))))


(fn read_file []
  (let [input (with-open [fin (io.open :days/3/day3.input)] ((fin:lines)))]
    input))

(walk (read_file))
(print (length coords))
