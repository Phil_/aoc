local M = {}

function M.split(inputstr, sep)
   if sep == nil then
      sep = "%s"
   end
   local t={}
   for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
      table.insert(t, str)
   end
   return t
end

-- read lines of a file to table
function M.readlines(filename)
    local arr = {}
    local f = assert(io.open(filename))
    local val = f:read()
    while val do
        table.insert(arr, val)
        val = f:read()
    end
    f:close()
    return arr
end

-- reverse a list
function M.reverse(t)
    local rev = {}
    for i=#t, 1, -1 do
        rev[#rev+1] = t[i]
    end
    return rev
end

return M
