final: prev:  rec {
  lua = prev.lua.override {
    packageOverrides = luaself: luaprev: {
      md5 = prev.callPackage ./md5.nix {};

      aoclib = luaprev.buildLuaPackage {
        pname = "aoclib";
        version = "1";
        src = ./aoc_util.lua;
        phases = [ "buildPhase" ];
        buildPhase = ''
          install -d $out/share/lua/${prev.lua.luaversion}
          install -m 644 $src $out/share/lua/${prev.lua.luaversion}/aoclib.lua
        '';
      };
    };
  };

  luaPackages = lua.pkgs;
}
