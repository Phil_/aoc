{ lua, fetchurl, fetchFromGitHub }:

let
  inherit (lua.pkgs) buildLuarocksPackage;
in buildLuarocksPackage rec {
  pname = "md5";
  version = "1.3-1";
  knownRockspec = (fetchurl {
    url = "https://luarocks.org/${pname}-${version}.rockspec";
    sha256 = "sha256-2S6Dvk0yazW8DCJf2DYxrLW11+B7r6DCJZ7CMCMAfSI=";
  }).outPath;

  src = fetchFromGitHub {
    owner = "keplerproject";
    repo = "md5";
    rev = "1.3";
    sha256 = "sha256-mCN7mmCGf2vdXriT1q8rrhUmKh49AY8A+cZVCu6XJzY=";
  };

  propagatedBuildInputs = [ lua ];
}
