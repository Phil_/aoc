{ pkgs
, inputs
, ...
}:

{
  haskell = {
    env = pkgs.haskellPackages.ghcWithHoogle (hPkgs: with hPkgs; [
      # libraries
      arrows async cgi criterion
      # tools
      cabal-install haskintex
      hoogleLocal
    ]);

    name = "haskell";
    extension = "hs";
    comment = "--";
  };

  python = {
    env = pkgs.python310.withPackages(ps: with ps; [
      matplotlib
      numpy
      pkgs.black
      pytest
    ]);

    name = "python";
    extension = "py";
    comment = "#";
  };

  lua = let
    luapkgs = pkgs.extend (import ./luaoverlay);
  in {
    env = luapkgs.lua.withPackages(ps: with ps; [
      readline
      md5
      aoclib
      luxio

      fennel
      readline
    ]);

    extraPackages = with luapkgs; [
      fnlfmt
    ];

    name = "lua";
    extension = "lua";
    comment = "--";
  };

  janet = {
    env = [
      pkgs.janet
      (pkgs.writeShellScriptBin "lsp" ''
        ${pkgs.janet}/bin/janet -e "(import spork/netrepl) (netrepl/server)"
      '')

      (pkgs.writeShellScriptBin "bench" ''
        YEAR="''${1:-$(date +%Y)}"
        DAYS=$(ls $YEAR/janet | tr '\n' ',' | sed 's/,$//')
        hyperfine -N --warmup 20 -L day $DAYS "janet $YEAR/janet/{day}/main.janet"
      '')
    ];

    template = ''
      (import aoc)
      (setdyn :year ''${year})
      (setdyn :day ''${day})
      (def lines (aoc/daylines))
    '';

    name = "janet";
    extension = "janet";
    comment = "#";
  };
}
