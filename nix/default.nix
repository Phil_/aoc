{ inputs
, system ? "x86_64-linux"
, ...
}@args:

rec {
  pkgs = args.pkgs or inputs.nixpkgs.legacyPackages.${system};

  mkShell = language: pkgs.mkShell {
    buildInputs = [
      language.env
      (scripts.aoc language)
      scripts.submit
    ] ++ (language.extraPackages  or []);
  };

  languages = import ./languages.nix {
    inherit (args) inputs;
    inherit pkgs;
  };

  scripts = {
    submit = pkgs.writeShellScriptBin "submit" ''
      #!/usr/bin/env bash

      set -e

      year=$1
      day=$2
      level=$3
      answer=$4
      cookie_file=.cookie

      if [ -z "$year" -o -z "$day" -o -z "$level" -o -z "$answer" ]; then
        echo "please provide all inputs properly"
      else
        if [ ! -f $cookie_file ] && [ -z "$AOC_COOKIE" ]; then
          echo -e "to get input automated, set the AOC_COOKIE env or paste your cookie into a file called ''${cookie_file} :)"
        else
          [ -z "$AOC_COOKIE" ] && AOC_COOKIE=$(cat $cookie_file)
          cookie="session=$AOC_COOKIE"

          url="https:/adventofcode.com/$year/day/$day/answer"
          data="level=$level&answer=$answer"

          curl -Ss -X POST $url -H "Content-Type: application/x-www-form-urlencoded" -d "$data" -b $cookie | \
            ${pkgs.pandoc}/bin/pandoc -f html -t markdown - | \
            sed -n '/role="main"/,/:::/p' | grep -v "^:::" | \
            sed -e 's/\\//g' | sed -e 's/\s+$//'
        fi
      fi
    '';

    aoc = { template ? "", extension, comment, name, ... }: pkgs.writeShellScriptBin "aoc" ''
      #!/usr/bin/env bash

      set -e

      # input
      year="$1"
      day="$2"

      # custom settings for language/comment symbol
      srcfile="main.${extension}"
      csymbol="${comment}"
      cookie_file=.cookie

      # -----------------

      # meta
      url="https://adventofcode.com/$year/day/$day"

      # files
      srcpath="$(printf '%d/${name}/day_%02d/%s' $year $day $srcfile)"
      outpath="$(printf '%d/inputs/day_%02d.txt' $year $day)"

      # check input
      if [ -z "$year" -o -z "$day" ]; then
        echo "provide both day and year please"
        exit 1
      else
        printf "fetching task + input file for %02d/%d to %s\n" $day $year $outpath
      fi

      # -----------------

      mkdir -p $(dirname $outpath)
      mkdir -p $(dirname $srcpath)

      # fetch and parse the instructions
      if [ ! -f "$srcpath" ]; then
        touch $srcpath
        ${pkgs.pandoc}/bin/pandoc -f html -t markdown $url | \
          sed -n '/##/,/To play/p' | \
          grep -v "To play" | sed -e "s/^/''${csymbol} /" | \
          sed -e 's/\\//g' | sed -e 's/\s+$//' > $srcpath
          cat << EOT >> $srcpath
  ${template}
  EOT
      fi

      if [ ! -f "$outpath" ]; then
        if [ ! -f $cookie_file ] && [ -z "$AOC_COOKIE" ]; then
          echo -e "to get input automated, set the AOC_COOKIE env or paste your cookie into a file called ''${cookie_file} :)" | tee $outpath
        else
          [ -z "$AOC_COOKIE" ] && AOC_COOKIE=$(cat "$cookie_file")
          cookie="session=$AOC_COOKIE"
          curl --fail -sS -b "$cookie" "$url/input"  -o "$outpath"
        fi
      fi

      # -----------------

      # open browser
      #${pkgs.xdg-utils}/bin/xdg-open $url

      # open editor
      $EDITOR $srcpath $outpath
    '';
  };
}
